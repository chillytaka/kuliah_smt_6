#include <avr/io.h>
#include <util/delay.h>

void delay_us(int delay)
{
	for (int i = 0; i < delay; i++)
	{
		_delay_us(1);
	}
}

int main(void)
{
	DDRD = 255; // port D (PD0 .. PD7) difungsikan sebagai output
	DDRB = 0b00000101;
	PORTB = 0b11111010;
	int delay_satu, delay_dua;
	int master = 500;
	delay_satu = master;
	delay_dua = master;
	uint8_t pin_PB1, pin_PB3;
awal:
	pin_PB1 = (PINB & (1 << PINB1)) >> PINB1;
	pin_PB3 = (PINB & (1 << PINB3)) >> PINB3;
	if (pin_PB1 == 1 && pin_PB3 == 0)
	{
		if (delay_satu < master * 2)
		{
			delay_satu += 1;
			delay_dua -= 1;
		}
	}
	else if (pin_PB1 == 0 && pin_PB3 == 1)
	{
		if (delay_dua < master * 2)
		{
			delay_satu -= 1;
			delay_dua += 1;
		}
	}

	if (delay_satu != 0)
	{
		PORTD = 255; // semua led on
		delay_us(delay_satu);
	}
	if (delay_dua != 0)
	{
		PORTD = 0; // semua led off
		delay_us(delay_dua);
	}

	goto awal; // ulangi mulai dari awal
}