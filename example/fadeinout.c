#include <util/delay.h>
#include <avr/io.h>

int main()
{
    TCCR0 |= (1 << WGM00) | (1 << WGM01); //Fast PWM Mode
    TCCR0 |= (1 << COM01);                //Low jika TCNT0 >= OCR0
    TCCR0 |= (1 << CS00);                 //No-Prescale
    TCCR1B |= (1 << WGM12);
    TCCR1A |= (1 << WGM10);
    TCCR1A |= (1 << COM1A1) | (1 << COM1B1);
    TCCR1B |= (1 << CS10);
    TCCR2 |= (1 << WGM20) | (1 << WGM21); //Fast PWM Mode
    TCCR2 |= (1 << COM21);                //Low jika TCNT0 >= OCR0
    TCCR2 |= (1 << CS20);                 //No-Prescale

ulang:
    DDRB = 0b00001000; //Set PB3 sebagai Output
    for (int i = 0; i < 255; i++)
    {
        OCR0 = i;
        _delay_ms(10);
    }

    for (int i = 255; i > 0; i--)
    {
        OCR0 = i;
        _delay_ms(10);
    }

    DDRB = 0b00000000; //Matikan PB3 sebagai Output
    DDRD = 0b00010000; //Set PD sebagai Output

    for (int i = 0; i < 255; i++)
    {
        OCR1B = i;
        _delay_ms(10);
    }

    for (int i = 255; i > 0; i--)
    {
        OCR1B = i;
        _delay_ms(10);
    }
    DDRD = 0b00100000;

    for (int i = 0; i < 255; i++)
    {
        OCR1A = i;
        _delay_ms(10);
    }

    for (int i = 255; i > 0; i--)
    {
        OCR1A = i;
        _delay_ms(10);
    }

    DDRD = 0b10000000;

    for (int i = 0; i < 255; i++)
    {
        OCR2 = i;
        _delay_ms(10);
    }

    for (int i = 255; i > 0; i--)
    {
        OCR2 = i;
        _delay_ms(10);
    }
    DDRD = 0b00000000;
    goto ulang;
}