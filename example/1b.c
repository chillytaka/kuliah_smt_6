//...................................................................... jawab1 p9 https://electronics.stackexchange.com/questions/5302/how-do-i-read-digital-input-on-atmega16

#include <avr/io.h>

int main(void)
{
	// set all pins on PORTB for output
	DDRD = 255;

	// set port pin PORTD2 as input and leave the others pins
	// in their originally state (inputs or outputs, it doesn't matter)
	DDRB = 0; //~(1 << PB2); // see comment #1 11111011
	PORTB = 255;

ulang:
{
	int pin_PB2 = (PINB & (1 << PINB2)) >> PINB2;

	if (!pin_PB2)
	{

		PORTD = 0b11111111; //(1<<PD2); // see comment #3 00000100
		goto ulang;
	} //(1<<PB2)) // see comment #2 PINB & 00000100

	PORTD = 0b00000000; //~(1<<PD2); // see comment #4 11111011
	goto ulang;
}
}