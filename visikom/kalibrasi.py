import numpy as np
import cv2.cv2 as cv2

criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
cap = cv2.VideoCapture(0)
objp = np.zeros((4*6, 3), np.float32)
objp[:, :2] = np.mgrid[0:6, 0:4].T.reshape(-1, 2)

objPoint = []
imgPoint = []
dst = []

while True:
    # Capture frame-by-frame
    ret, frame = cap.read()

    """
    get Point to Calibrate Camera
    """

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    ret2, corners = cv2.findChessboardCorners(gray, (6, 4), None)

    if ret2:
        objPoint.append(objp)

        corners2 = cv2.cornerSubPix(
            gray, corners, (11, 11), (-1, -1), criteria)

        imgPoint.append(corners2)

        img = cv2.drawChessboardCorners(frame, (6, 4), corners2, ret2)

        if (cv2.waitKey(1) & 0xFF == ord('a')):

            print("get dist...")
            ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(
                objPoint, imgPoint, gray.shape[::-1], None, None)

            h, w = frame.shape[:2]
            print("get roi...")
            newcameramtx, roi = cv2.getOptimalNewCameraMatrix(
                mtx, dist, (w, h), 1, (w, h))

            print("undistoring......")
            dst = cv2.undistort(frame, mtx, dist, None, newcameramtx)
            x, y, w, h = roi
            dst = dst[y:y+h, x:x+w]

            print("saving & preview......")
            cv2.imshow("result", dst)
            cv2.imwrite("resoult.png", dst)

            print("resetting")
            objPoint = []
            imgPoint = []

    """
    end of get point to calibrate camera
    """

    cv2.imshow("frame1", frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
