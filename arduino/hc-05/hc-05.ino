// This program shown how to control arduino from PC Via Bluetooth
// Connect ...
// arduino>>bluetooth
// D11   >>>  Rx
// D10   >>>  Tx
//Written By Mohannad Rawashdeh

#include <SoftwareSerial.h>// import the serial library

#define heaterOn 49 //number 1 in ascii
#define heaterOff 48 //number 0 in ascii
#define chairOccupied 2
#define chairUnoccupied 3
#define commandSuccess 9

SoftwareSerial HC05(10, 11); // RX, TX
const int ledpin=13; // led on D13 will show blink on / off
const int buttonPin = 4;
int BluetoothData; // the data given from Computer
int buttonState = 0;

void setup() {
  // put your setup code here, to run once:
  HC05.begin(9600);
  Serial.begin(9600);
  pinMode(ledpin,OUTPUT);
  pinMode(buttonPin,INPUT);
}

void loop() {
  // put your main code here, to run repeatedly: 
  if (HC05.available()) {
    
    BluetoothData=HC05.read();
    Serial.println(BluetoothData);
      
    if(BluetoothData == 49) {   // if number 1 pressed ....
      
      digitalWrite(ledpin,1);
      HC05.println(commandSuccess);
    }
       
    if (BluetoothData==heaterOff) {// if number 0 pressed ....
      
      digitalWrite(ledpin,0);
      HC05.println(commandSuccess);\
      
    }
    
  }

  buttonState = digitalRead(buttonPin);

  if (buttonState == HIGH ) {
    HC05.println(chairOccupied);
  }
  
  if (buttonState == LOW) {
       HC05.println(chairUnoccupied);
    }
    
  delay(100);// prepare for next data ...
}
