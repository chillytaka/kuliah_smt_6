//3
#include <avr/io.h>
#include <avr/interrupt.h>
volatile uint8_t countlower, countupper;

ISR(TIMER1_OVF_vect)
{
    if (countupper == 2)
    {
        PORTB |= 0b00001111;
    }
    if (countupper == 4)
    {
        PORTB &= 0b11110000;
        countupper = 0;
    }
    if (countlower == 4)
    {
        PORTB |= ~0b00001111;
    }
    if (countlower == 8)
    {
        PORTB &= ~0b11110000;
        countlower = 0;
    }
    countupper++;
    countlower++;
    TCNT1 = 65047;
}

int main(void)
{
    DDRB = 0377; //oktal
    TCNT1 = 65047;
    TCCR1A = 0;
    TCCR1B = (1 << CS10) | (1 << CS12); //clk I/O /1024 (From prescaler)
    TIMSK = (1 << TOIE1);               //Overflow Interrupt Enable
    sei();
giti:
    goto giti;
}