
import cv2.cv2 as cv2
import os
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QDialog
from PyQt5 import QtCore, QtGui, QtWidgets
import threading
import numpy as np
from threading import Timer, Thread, Event
import ctypes  # An included library with Python install.
import copy
import os
import glob
from PyQt5.QtWidgets import QMessageBox

from datetime import datetime


class cSelectCamera(QDialog):
    def __init__(self):
        super().__init__()
        self.NoKamera = 0
        self.pGambar = None
        self.thread = None
        self.cap = None
        self.thread = None
        self.started = False
        self.NamaKamera = ""
        self.initUI()
        self.bef = " "
        self.t = 1
        self.c = 0
        self.threadd = Timer(self.t, self.StartCapture)
        self.threadd.start()
        self.Proses = False
        self.NoPilih = -1
        self.Mode = 0
        self.bStartKalibrasi = False
        print(1)

    def initUI(self):
       # self.setObjectName("MainWindow")
        self.setWindowTitle("Select Camera Port")
        self.setFixedSize(700, 600)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        dn = 25
        n = 10
        self.pnmKamera = QtWidgets.QLabel(self)
        self.pnmKamera.setGeometry(QtCore.QRect(30, n, 400, 20))
        self.pnmKamera.setText("Nama Kamera :  "+self.NamaKamera)
        n = n+dn

        self.pKamera = QtWidgets.QLabel(self)
        self.pKamera.setGeometry(QtCore.QRect(30, n, 100, 20))
        self.pKamera.setText("Nomor Port     :")

        self.cNoKamera = QtWidgets.QComboBox(self)
        self.cNoKamera.setGeometry(QtCore.QRect(30+120, n, 300, 20))
        self.cNoKamera.addItems(["None", "0", "1", "2", "3", "4", "5"])
        self.ModeKalibrasiKamera(0, "Kamera")
        # self.cNoKamera.activated[str].connect(self.cBSelectNoKamera)
        n = n+dn

        self.lbstatus = QtWidgets.QLabel(self)
        self.lbstatus.setGeometry(QtCore.QRect(30, n, 600, 20))
        self.lbstatus.setText("Status      :")

        n = n+dn
        self.pt = QtWidgets.QLabel(self)
        self.pt.setGeometry(QtCore.QRect(30, n, 100, 20))
        self.pt.setText("Tes Kamera:")
        n = n+dn
        self.pGambar = QtWidgets.QLabel(self)
        self.pGambar.setGeometry(QtCore.QRect(30, n, 800, 400))
        self.DrawBlank()
        n = n+dn+390
        self.BtSave = QtWidgets.QPushButton("Save And Exit", self)
        self.BtSave.setGeometry(QtCore.QRect(30, n, 150, 28))
        self.BtSave.clicked.connect(self.SaveAndExit)

        self.BtCancel = QtWidgets.QPushButton("Cancel", self)
        self.BtCancel.setGeometry(QtCore.QRect(30+160, n, 150, 28))
        self.BtCancel.clicked.connect(self.Cancel)

    def StartCapture(self):
        self.threadd.cancel()
        self.thread = threading.Thread(target=self.CaptureVideo(), args=())
        self.thread.start()

    def ModeSettingKamera(self, NoKamera, NamaKamera):
        self.Mode = 0
        self.NamaKamera = NamaKamera
        self.BtSave.setText("Save And Exit")
        self.pnmKamera.setText("Nama Kamera :  "+self.NamaKamera)
        self.NoKamera = NoKamera
        self.cNoKamera.setEnabled(True)
        self.bStartKalibrasi = False
        self.cNoKamera.setCurrentIndex(NoKamera+1)

    def ModeKalibrasiKamera(self, NoKamera, NamaKamera):
        self.Mode = 1
        self.NamaKamera = NamaKamera
        self.NoKamera = NoKamera
        # self.lbstatus.setText("Status      : Menunggu Kalibrasi Kamera")
        # self.BtSave.setText("Mulai Kalibrasi")
        self.pnmKamera.setText("Nama Kamera :  "+self.NamaKamera)
        self.cNoKamera.setEnabled(True)
        self.cNoKamera.setCurrentIndex(NoKamera+1)
        self.bStartKalibrasi = False
        self.cNoKamera.setEnabled(False)

    def DrawBlank(self):
        rgb_color = (0, 0, 0)
        image = np.zeros((480, 640, 3), np.uint8)
        color = tuple(reversed(rgb_color))
        image[:] = color
        image = QtGui.QImage(
            image.data, image.shape[1], image.shape[0], QtGui.QImage.Format_RGB888).rgbSwapped()
        self.pGambar.setPixmap(QtGui.QPixmap.fromImage(image))

    def cBSelectNoKamera(self):
        self.threadd.cancel()
        self.NoKamera = self.cNoKamera.currentIndex()-1
        self.started = False
        self.StartCapture()

    def CekKamera(self, NoKamera):
        T = True
        try:
            cap = cv2.VideoCapture(NoKamera)
            ret, frame = cap.read()
            cap.release()
        except:
            T = False
        return T

    def GetNoKamera(self):
        return self.NoPilih

    def Cancel(self):
        self.started = False
        print(self.cap)

        """         if self.cap != None:
            print("test")
            self.cap.release() """
        self.NoKamera = -1

        self.close()

    def StopCapture(self):
        self.started = False
        if self.cap != None:
            self.cap.release()

    def SaveAndExit(self):
        if self.Mode == 0:
            self.started = False
            # self.StopCapture()
            self.NoKamera = self.cNoKamera.currentIndex()-1
            self.NoPilih = self.NoKamera
            self.close()
        else:
            self.bStartKalibrasi = not self.bStartKalibrasi
            if self.BtStartKalibrasi:
                ret = QMessageBox.question(
                    self, ' ', "Menghapus data kalibrasi "+self.NamaKamera, QMessageBox.Yes | QMessageBox.No)
                if ret == QMessageBox.Yes:
                    self.DeleteDataFile()
            else:
                pass
            if self.bStartKalibrasi:
                self.lbstatus.setText(
                    "Status      : Kalibrasi Kamera Sedang Berlangsung")
                self.BtSave.setText("Berhentikan Kalibrasi")
            else:
                self.lbstatus.setText(
                    "Status      : Menunggu Kalibrasi Kamera")
                self.BtSave.setText("Mulai Kalibrasi")

            self.threadd.cancel()
            self.started = False
            self.StartCapture()

    def KetTidakDitemukan(self):
        if self.NoKamera > -1:
            self.lbstatus.setText(
                "Status : Port Kamera Nomor "+str(self.NoKamera)+" Tidak Ditemukan")
        else:
            self.lbstatus.setText(
                "Status : Tidak ada Port Kamera  yang Dipilih")

    def KetDitemukan(self):
        self.lbstatus.setText("Status : Port Kamera Nomor " +
                              str(self.NoKamera)+" ' Ditemukan")

    def CaptureVideo(self):
        if self.Mode == 0:
            self.CaptureVideoSetting()
        else:
            if self.bStartKalibrasi:
                self.CaptureKalibrasi()
            else:
                self.CaptureVideoSetting()

    def CaptureVideoSetting(self):
        if self.started == False:
            try:
                if self.cap != None:
                    self.cap.release()
            except:
                return
            self.cap = cv2.VideoCapture(self.NoKamera)
            ret, frame = self.cap.read()
            self.cap.release()

            if ret == False:
                self.KetTidakDitemukan()
                self.DrawBlank()
                return
            self.started = True
            self.KetDitemukan()
            self.Proses = True
            self.cap = cv2.VideoCapture(self.NoKamera)
            dsize = (640, 480)
            while (self.started):
                ret, frame = self.cap.read()
                frame = cv2.resize(frame, dsize)
                frame = QtGui.QImage(
                    frame.data, frame.shape[1], frame.shape[0], QtGui.QImage.Format_RGB888).rgbSwapped()
                self.pGambar.setPixmap(QtGui.QPixmap.fromImage(frame))
                cv2.waitKey(2)
            self.cap.release()
            self.Proses = False
        else:
            self.started = False

    def CreateDirectory(self):
        sf = os.getcwd()+"\\"+self.NamaKamera
        if not os.path.exists(sf):
            os.makedirs(sf)

    def DeleteDataFile(self):

        sf = os.getcwd()+"\\"+self.NamaKamera+"\\*.jpg"
        files = glob.glob(sf)
        for f in files:
            try:
                f.unlink()
            except:
                pass

        sf = os.getcwd()+"\\"+self.NamaKamera+"\\*.txt"
        files = glob.glob(sf)
        for f in files:
            try:
                f.unlink()
            except:
                pass

    def CaptureKalibrasi(self):
        if self.started == False:
            try:
                if self.cap != None:
                    self.cap.release()
            except:
                return
            if not((self.CekKamera(self.NoKamera))):
                self.KetTidakDitemukan()
                self.DrawBlank()
                return
            self.started = True
            self.KetDitemukan()
            self.Proses = True
            self.cap = cv2.VideoCapture(self.NoKamera)
            CHECKERBOARD = (6, 8)
            dsize = (640, 480)
            now1 = datetime.now()
            now2 = datetime.now()
            self.CreateDirectory()
            curdir = os.getcwd()+"\\"+self.NamaKamera
            while (self.started):
                ret, frame = self.cap.read()
                frameOri = copy.copy(frame)
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                ret, corners = cv2.findChessboardCorners(
                    gray, CHECKERBOARD, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FAST_CHECK + cv2.CALIB_CB_NORMALIZE_IMAGE)
                if ret == True:
                    frame = cv2.drawChessboardCorners(
                        frame, CHECKERBOARD, corners, ret)
                    now2 = datetime.now()
                    d = now2-now1
                    if d.seconds >= 4:
                        now1 = datetime.now()
                        f = now1.strftime("%Y%M%d%H%M%S")
                        sf = curdir+"\\"+f+".jpg"
                        cv2.imwrite(sf, frameOri)
                frame = cv2.resize(frame, dsize)
                frame = QtGui.QImage(
                    frame.data, frame.shape[1], frame.shape[0], QtGui.QImage.Format_RGB888).rgbSwapped()
                self.pGambar.setPixmap(QtGui.QPixmap.fromImage(frame))
                cv2.waitKey(2)
            self.cap.release()
            # self.ProsesKalibrasi()
            self.Proses = False
        else:
            self.started = False

    def closeEvent(self, event):
        self.cNoKamera.setEnabled(True)
        self.started = False
        self.threadd.cancel()

    def showEvent(self, event):
        if self.CekKamera(self.NoKamera):
            self.threadd = Timer(self.t, self.StartCapture)
            self.threadd.start()
        pass


class cSettingCamera(QMainWindow):
    def __init__(self):
        super().__init__()
        self.NoKamera = 0
        self.pGambar = None
        self.thread = None
        self.cap = None
        self.thread = None
        self.started = False
        self.NamaKamera = "Kamera-A"
        # self.timer =0
        self.initUI()
        self.d = None

    def initUI(self):
        self.setWindowTitle("Setting Camera Port")
        self.setFixedSize(600, 300)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        dn = 30
        n = 10
        self.h = ["Kamera-A",
                  "Kamera-B",
                  "Kamera-C",
                  "Kamera-D",
                  "Kamera-E",
                  "Kamera-F", ]
        self.l = []
        self.lp = []
        self.p = []
        self.lb = []

        self.lport = self.LoadFile("Port.txt")
        print(self.lport)
        if not self.lport:
            for i in range(0, 5):
                self.lport.append(-1)
            self.SaveFile("Port.txt", self.lport)

        for i in range(0, 5):

            n = n+dn
            self.l.append(QtWidgets.QLabel(self))
            self.l[i].setGeometry(QtCore.QRect(30, n, 400, 20))
            self.l[i].setText(self.h[i])
            self.l[i].setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))

            self.lp.append(QtWidgets.QLabel(self))
            self.lp[i].setGeometry(QtCore.QRect(300, n, 400, 20))
            if self.lport[i] == -1:
                self.lp[i].setText("Port : None")

            else:
                self.lp[i].setText("Port :"+str(self.lport[i]))

            self.lp[i].setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
            self.lb.append(QtWidgets.QPushButton("Set", self))
            self.lb[i].setGeometry(QtCore.QRect(30+400, n, 150, 28))

        self.lb[0].clicked.connect(lambda: self.bC(0))
        self.lb[1].clicked.connect(lambda: self.bC(1))
        self.lb[2].clicked.connect(lambda: self.bC(2))
        self.lb[3].clicked.connect(lambda: self.bC(3))
        self.lb[4].clicked.connect(lambda: self.bC(4))
        n = n+dn + 40
        self.BtSave = QtWidgets.QPushButton("Set Port And Exit", self)
        self.BtSave.setGeometry(QtCore.QRect(30, n, 150, 28))
        self.BtSave.clicked.connect(self.SaveAndExit)

        self.BtCancel = QtWidgets.QPushButton("Cancel", self)
        self.BtCancel.setGeometry(QtCore.QRect(30+160, n, 150, 28))
        # self.BtCancel.clicked.connect(self.Cancel)

    def bC(self, no):

        self.d = cSelectCamera()
        self.d.ModeSettingKamera(self.lport[no], self.h[no])
        self.d.exec()
        nn = self.d.GetNoKamera()
        if nn > -2:
            self.lport[no] = nn
            if self.lport[no] > -1:
                self.lp[no].setText("Port : "+str(self.lport[no]))
            else:
                self.lp[no].setText("Port : None")

    def LoadFile(self, NmFile, kD=0):
        sf = os.getcwd()+"/"+NmFile
        ll = []
        if os.path.exists(sf):
            with open(sf) as f:
                lines = f.read().splitlines()
            if kD == 0:
                for s in lines:
                    ll.append(int(s))
            else:
                ll = lines

        return ll

    def SaveFile(self, NmFile, ll):
        sf = os.getcwd()+"/"+NmFile
        print(sf)
        f = open(sf, 'w')
        for ele in ll:
            f.write(str(ele)+'\n')
        f.close()

    def SaveAndExit(self):
        ll = []
        for prt in self.lport:
            ll.append(str(prt))
        self.SaveFile("Port.txt", ll)
        self.close()
#  >>> x = y = z = np.arange(0.0,5.0,1.0)
# >>> np.savetxt('test.out', x, delimiter=',')   # X is an array
# >>> np.savetxt('test.out', (x,y,z))   # x,y,z equal sized 1D arrays
# >>> np.savetxt('test.out', x, fmt='%1.4e')   # use exponential notation


def ProsesKalibrasi():
    NamaKamera = "Kamera"

    sdir = os.getcwd()+"\\"+NamaKamera+"\\*.jpg"

    CHECKERBOARD = (6, 8)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    objpoints = []
    imgpoints = []
    objp = np.zeros((1, CHECKERBOARD[0] * CHECKERBOARD[1], 3), np.float32)
    objp[0, :, :2] = np.mgrid[0:CHECKERBOARD[0],
                              0:CHECKERBOARD[1]].T.reshape(-1, 2)
    images = glob.glob(sdir)
    if len(images) > 0:
        for fname in images:
            frame = cv2.imread(fname)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            ret, corners = cv2.findChessboardCorners(
                gray, CHECKERBOARD, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FAST_CHECK + cv2.CALIB_CB_NORMALIZE_IMAGE)
            if ret == True:
                objpoints.append(objp)
                corners2 = cv2.cornerSubPix(
                    gray, corners, (11, 11), (-1, -1), criteria)
                imgpoints.append(corners2)
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(
            objpoints, imgpoints, gray.shape[::-1], None, None)
        i = 0
        sf = os.getcwd()+"\\"+NamaKamera+"\\mtx.txt"
        np.savetxt(sf, mtx, delimiter=',')
        sf = os.getcwd()+"\\"+NamaKamera+"\\ret.txt"
        sf = os.getcwd()+"\\"+NamaKamera+"\\dist.txt"
        np.savetxt(sf, dist, delimiter=',')

        for fname in images:
            nf = os.path.splitext(os.path.basename(fname))[0]+"-rvecs.txt"
            sf = os.getcwd()+"\\"+NamaKamera+"\\"+nf
            np.savetxt(sf, rvecs[i], delimiter=',')
            nf = os.path.splitext(os.path.basename(fname))[0]+"-tvecs.txt"
            sf = os.getcwd()+"\\"+NamaKamera+"\\"+nf
            np.savetxt(sf, tvecs[i], delimiter=',')
            i = i+1


# =============================================================================
#         sf = os.getcwd()+"\\"+NamaKamera+"\\mtr.txt"
#         np.savetxt(sf, mtx, delimiter=',')
#
#         sf = os.getcwd()+"\\"+NamaKamera+"\\ret.txt"
#         np.savetxt(sf, ret, delimiter=',')
#
#         sf = os.getcwd()+"\\"+NamaKamera+"\\rvecs.txt"
#         np.savetxt(sf, rvecs, delimiter=',')
#
#
#         sf = os.getcwd()+"\\"+NamaKamera+"\\tvecs.txt"
#         np.savetxt(sf, tvecs, delimiter=',')
# =============================================================================


def main():
    app = QApplication(sys.argv)

    # main=cSelectCamera()
    # main.ModeKalibrasiKamera(0,"Kamera")
    main = cSettingCamera()
    main.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    #     ProsesKalibrasi()
    main()
