#include <util/delay.h>
#include <stdlib.h>
#include <avr/io.h>

int main()
{
	TCCR1B |= (1 << WGM12); //Fast PWM Mode
	TCCR1A |= (1 << WGM10);
	TCCR1A |= (1 << COM1A1) | (1 << COM1B1); //Low jika TCNT0 >= OCR0
	TCCR1B |= (1 << CS10);					 //No-Prescale
	DDRD = 0b00010000;						 //Set PB3 sebagai Output
	for (int i = 0; i < 255; i++)
	{
		OCR1B = i;
		_delay_ms(10);
	}

	for (int i = 255; i > 0; i--)
	{
		OCR1B = i;
		_delay_ms(10);
	}

	DDRD = 0b00100000; //Set PB3 sebagai Output
	for (int i = 0; i < 255; i++)
	{
		OCR1A = i;
		_delay_ms(10);
	}

	for (int i = 255; i > 0; i--)
	{
		OCR1A = i;
		_delay_ms(10);
	}

usai:
	goto usai;
}