import cv2.cv2 as cv2  # removing intellisense error...
import os
import copy
from datetime import datetime
NoKamera = 0

if not os.path.exists('./result/'):
    os.makedirs('./result/')
if not os.path.exists('./result/'+str(NoKamera)):
    os.makedirs('./result/'+str(NoKamera))

cap = cv2.VideoCapture(NoKamera)
#!setx OPENCV_VIDEOIO_PRIORITY_MSMF 0
CHECKERBOARD = (4, 6)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
now1 = datetime.now()
now2 = datetime.now()
while True:
    ret, frame = cap.read()
    frameOri = copy.copy(frame)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    ret, corners = cv2.findChessboardCorners(
        gray, CHECKERBOARD, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FAST_CHECK + cv2.CALIB_CB_NORMALIZE_IMAGE)
    if ret == True:
        frame = cv2.drawChessboardCorners(frame, CHECKERBOARD, corners, ret)
        now2 = datetime.now()
        d = now2-now1
        if d.seconds >= 4:
            now1 = datetime.now()
            f = now1.strftime("%Y%m%d%H%M%S")
            sf = "./result/"+str(NoKamera)+"\\"+f+".jpg"
            print("saving...")
            cv2.imwrite(sf, frameOri)
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

print(1)
cap.release()
cv2.destroyAllWindows()
