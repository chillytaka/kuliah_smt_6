#include <avr/io.h>

int main(void)
{
	DDRB = 0;
	PORTB = 0;
	DDRD = 255;
	int counter = 0;
	int old_out = 0;

repeat:

	if ((PINB & 0b00000100) == 4)
	{
		if (counter >= 1000)
		{

			PORTD = 255;
			old_out = 255;
			counter++;
			goto repeat;
		}
		counter++;
		PORTD = old_out;
		goto repeat;
	}
	else
	{
		PORTD = 0;
		old_out = 0;
		counter = 0;
		old_pin = (PINB & 0b00000100);
		goto repeat;
		// PORTD = (PINB & 0b00000100) * 255;
	}
}